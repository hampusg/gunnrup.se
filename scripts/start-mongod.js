/* eslint import/no-extraneous-dependencies: off */
const Mongod = require('mongod');

const server = new Mongod(27017);
server.open((err) => {
  if (err === null) {
    console.log('Mongod deamon started via npm.');
  }
});
