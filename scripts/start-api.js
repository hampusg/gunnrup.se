/**
 * This script solves the problem of cross platform starting the api server.
 */

/* Translates to cd api && npm start (linux/unix environment) */
const args = ['start'];
const opts = { stio: 'inherit', cwd: 'api', shell: true };
require('child_process').spawn('npm', args, opts);
