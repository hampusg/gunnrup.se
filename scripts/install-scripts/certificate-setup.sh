#!/bin/sh
#########################################################
# Author: Hampus Gunnrup
# Date: 2018-07-31
# Updated: 2018-08-04
#
# This script sets up a the certificate and autorenewal process on a gunnrup.se server node.
# This script should be run after everything has been installed, and the gunnrup.se domain is pointing to the server.
#
#########################################################

# Setup certbot
sudo apt-get install software-properties-common
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install python-certbot-apache

# Setup certificate
sudo certbot --apache


# TESTING - BEGIN

  # Test Case 1
    # Action: Run 'ls /etc/cron.d'
    # Assert: Certbot is in the list
  # Test Case 2
    # Action: Run 'systemctl list-timers'
    # Assert: Certbot is visible
	# Test Case 3
    # Action: Navigate to https://gunnrup.se
    # Assert: The website is securely served
  # Test Case 4
    # Action: Navigate to http://gunnrup.se
    # Assert: You are redirected to https://gunnrup.se

# TESTING - END
