#!/bin/sh
#########################################################
# Author: Hampus Gunnrup
# Date: 2018-01-22
# Updated: 2018-08-05
#
# This script installs and configures the basic software that all nodes should include
# on an Ubuntu server (16.04).
# needed for the "gunnrup.se" project of Hampus Gunnrup.
#
#########################################################

# Make sure the firewall is installed.
# This step is not required for Ubuntu server (16.04)
# but it may have been uninstalled for some reason
sudo apt-get install -y ufw

# Set up the firewall rules
sudo ufw default deny incoming  # Ignore all incoming network by default
sudo ufw default allow outgoing # Allow all outgoing network by default
sudo ufw allow ssh 			    	 	# Allow shh in both directions, you could use 22  as well, instead of ssh

# Start the firewall
sudo ufw --force enable

# Ensure that systemd is installed
sudo apt-get install systemd

# If needed, get the token username
if [ -z ${tokenUsername+x} ]; then 
	read -p "Enter the deploy token username: " tokenUsername;
fi
# If needed, get the token password
if [ -z ${tokenPassword+x} ]; then 
	read -p "Enter the deploy token password: " tokenPassword
fi

# Configure git user information
git config --global user.name "gunnrup.se-server-node"
git config --global user.email "admin@gunnrup.se"

# Setup credentials for the repo
cd ~/gunnrup.se
git remote set-url origin https://$tokenUsername:$tokenPassword@gitlab.com/hgsoftware-gunnrup.se/gunnrup.se.git


# TESTING - BEGIN

	# Test Case 1
		# Action: Run 'sudo ufw status'
		# Assert: The rules show port 22 set to allow
		# Assert: The firewall is enabled
	# Test Case 2
		# Action: Run 'cd ~/gunnrup.se; git pull'
		# Assert: The console logs "Already up to date."

# TESTING - END
