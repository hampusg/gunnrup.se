#!/bin/sh
#########################################################
# Author: Hampus Gunnrup
# Date: 2018-05-13
# Updated: 2018-08-04
#
# This script builds and adds the front-end of the gunnrup.se
# website.
# The script assumes that the "web-server-setup.sh" script has been run.
#
#########################################################

# Import sites in to /etc/apache2/sites-available
cd ~/gunnrup.se/configuration
git pull
sudo cp gunnrup.se.conf /etc/apache2/sites-available/gunnrup.se.conf
sudo cp staging.gunnrup.se.conf /etc/apache2/sites-available/staging.gunnrup.se.conf

# Build the website
cd ~/gunnrup.se/client
npm install
REACT_APP_API_LOCATION=/api npm run build

# Copy the website
sudo mkdir -p /var/www/gunnrup.se/production/client
sudo cp -a ~/gunnrup.se/client/build/. /var/www/gunnrup.se/production/client

# Build the website for staging
cd ~/gunnrup.se/client
npm install
REACT_APP_API_LOCATION=/staging-api npm run build

# Go to the home folder, as a good practice
cd ~

# Copy the staging website
sudo mkdir -p /var/www/gunnrup.se/staging/client
sudo cp -a ~/gunnrup.se/client/build/. /var/www/gunnrup.se/staging/client

# Disable default page
sudo a2dissite 000-default.conf

# Enable sites
sudo a2ensite gunnrup.se
sudo a2ensite staging.gunnrup.se

# Restart apache
sudo service apache2 restart
sudo service apache2 reload


# TESTING - BEGIN

	# Test Case 1:
		# Action: Run 'sudo ls /etc/apache2/sites-enabled'
		# Assert: The files gunnrup.se.conf and staging.gunnrup.se.conf exist in the list
		# Assert: The files 000-default.conf does not exist in the list
	# Test Case 2
		# Action: Modify /etc/apache2/sites-enabled/gunnrup.se.conf to not redirect to https://gunnrup.se
		# Action: Run 'sudo service apache2 reload'
		# Action: Add the text 'production version' in the body of the file /var/www/gunnrup.se/production/client/index.html
		# Action: Go to [server-ip] in a browser
		# Assert: The webpage should be shown, and the site document should contain the text 'production version' under the body tag
  # Test Case 3
		# Action: Run 'sudo a2dissite gunnrup.se'
		# Action: Modify /etc/apache2/sites-enabled/staging.gunnrup.se.conf to not redirect to https://staging.gunnrup.se
		# Action: Run 'sudo service apache2 reload'
		# Action: Add the text 'staging version' in the body of the file /var/www/gunnrup.se/staging/client/index.html
		# Action: Go to [server-ip] in a browser
		# Assert: The webpage should be shown, and the site document should contain the text 'staging version' under the body tag
	# Test Case 4:
		# Action: Run 'sudo ls /var/www/gunnrup.se/production/client'
		# Assert: At least an index.html file exist in the list
	# Test Case 5:
		# Action: Run 'sudo ls /var/www/gunnrup.se/staging/client'
		# Assert: At least an index.html file exist in the list

# TESTING - END
