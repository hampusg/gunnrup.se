#!/bin/sh
#########################################################
# Author: Hampus Gunnrup
# Date: 2018-08-02
# Updated: 2018-08-02
#
# This script installs the required database software to a server node.
# The script assumes that the "web-server-setup.sh" script has been run.
#
#########################################################

# Import the public key
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927

# Create a list file for MongoDB
echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list

# Reload the local package database
sudo apt-get update

# Install mongodb
sudo apt-get install -y mongodb-org

#sudo service mongod start
sudo systemctl start mongod
sudo systemctl enable mongod

# Initialize the gunnrup.se database
mongo ~/gunnrup.se/api/configuration/init.mongo.js


# TESTING - BEGIN

  # Test Case 1
    # Action: Run 'sudo systemctl status mongod'
    # Assert: mongo is active
  # Test Case 2
    # Action: Start mongo shell and run the command 'show dbs'
    # Assert: The database gunnrup should be listed
  # Test Case 3
    # Action: Start mongo shell and run the command 'use gunnrup' and 'show collections'
    # Assert: The collection messages should be listed

# TESTING - END
