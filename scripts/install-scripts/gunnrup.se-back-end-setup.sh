#!/bin/sh
#########################################################
# Author: Hampus Gunnrup
# Date: 2018-05-13
# Updated: 2018-08-03
#
# This script builds and adds the back-end of the gunnrup.se
# website.
# The script assumes that the "web-server-setup.sh" script has been run.
#
#########################################################

# Copy the api
sudo mkdir -p /var/www/gunnrup.se/production/api
sudo cp -a ~/gunnrup.se/api/. /var/www/gunnrup.se/production/api
cd /var/www/gunnrup.se/production/api
npm install
PORT=3001 npm run launch
cd ~

# Copy the staging api
sudo mkdir -p /var/www/gunnrup.se/staging/api
sudo cp -a ~/gunnrup.se/api/. /var/www/gunnrup.se/staging/api
cd /var/www/gunnrup.se/staging/api
npm install
PORT=3002 npm run launch
cd ~

# TESTING - BEGIN

	# Test Case 1
		# Action: Run 'ls /var/www/gunnrup.se/production/api'
		# Assert: The api files should be listed
  # Test Case 2
		# Action: Run 'ls /var/www/gunnrup.se/staging/api'
		# Assert: The api files should be listed
	# Test Case 3:
		# Action: Run 'curl localhost:3001/api/messages'
		# Assert: The command returns no errors (i.e. {"records":[]})
	# Test Case 4:
		# Action: Run 'curl localhost:3002/api/messages'
		# Assert: The command returns no errors (i.e. {"records":[]})
	# Test Case 5:
		# Action: Run 'ps aux | grep forever'
		# Assert: Two processes are listed

# TESTING - END
