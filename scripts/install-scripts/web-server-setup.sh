#!/bin/sh
#########################################################
# Author: Hampus Gunnrup
# Date: 2018-01-23
# Updated: 2018-08-04
#
# This script installs and configures the web server components
# needed for the gunnrup.se project
# on an Ubuntu server (16.04).
# The script assumes that the "basic-server-setup.sh" script has been run.
#
#########################################################

# Set up firewall rules
sudo ufw allow http
sudo ufw allow https

# Install apache
sudo apt-get install -y apache2

# Enable reverse proxy
sudo a2enmod proxy
sudo a2enmod proxy_http
sudo a2enmod proxy_balancer
sudo a2enmod lbmethod_byrequests

# Enable rewrites
sudo a2enmod rewrite

# Reload and restart to make sure that everything is enabled
sudo service apache2 reload
sudo service apache2 restart

# Install nvm and then node
git clone https://github.com/creationix/nvm.git
nvm/install.sh
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
nvm install 8.11.1 # Install the latest LTS version of node

# The following line make node and npm available to sudo and bash
sudo ln -s /home/ubuntu/.nvm/versions/node/v8.11.1/bin/node /usr/bin/node
sudo ln -s /home/ubuntu/.nvm/versions/node/v8.11.1/bin/node /usr/lib/node
sudo ln -s /home/ubuntu/.nvm/versions/node/v8.11.1/bin/npm /usr/bin/npm
sudo ln -s /home/ubuntu/.nvm/versions/node/v8.11.1/bin/node-waf /usr/bin/node-waf

# Reload profile in order for npm and node to be registered
. ~/.profile


# TESTING - BEGIN

	# Test Case 1
		# Action: Run 'sudo ufw status'
		# Assert: The rules show http and https set to allow
		# Assert: The firewall is enabled
  # Test Case 2
    # Action: Run 'sudo service apache2 status'
    # Assert: The apache2 service is active
  # Test Case 3
    # Action: Run 'sudo apache2ctl -M'
    # Assert: proxy_module, proxy_http_module, proxy_balancer_module and lbmethod_byrequests_module are all in the displayed list
  # Test Case 4
    # Action: Run '. ~/.profile'
    # Action: Run 'node --version'
    # Action: Run 'npm --version'
    # Assert: The versions displayed matches the latest node LTS version

# TESTING - END
