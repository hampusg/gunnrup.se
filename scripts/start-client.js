/**
 * This script solves the problem of cross platform starting the client server.
 */

/* Translates to cd gunnrup-client && npm start (linux/unix environment) */
const args = ['start'];
const opts = { stio: 'inherit', cwd: 'client', shell: true };
require('child_process').spawn('npm', args, opts);
