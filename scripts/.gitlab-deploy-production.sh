#!/bin/bash

# This file is based on: https://medium.com/@lucabecchetti/autodeploy-from-gitlab-to-multiple-aws-ec2-instances-a43448727c5a 

# Get the servers list
set -f
string=$DEPLOY_SERVER
array=(${string//,/ })

# Build and install the client
cd client
npm install
REACT_APP_API_LOCATION=/api npm run build
cd ..

# Iterate over servers to deploy to and pull last commit
for i in "${!array[@]}"; do
  echo "Deploying to ${array[i]}"

  # Clear the current staging files
  ssh admin_gunnrup_se@${array[i]} "
    sudo rm -r /var/www/gunnrup.se/production/client/*
    sudo rm -r /var/www/gunnrup.se/production/api/*
  "
  # Copy the client and api code
  scp -r client/build/. admin_gunnrup_se@${array[i]}:/var/www/gunnrup.se/production/client
  scp -r api/. admin_gunnrup_se@${array[i]}:/var/www/gunnrup.se/production/api
  
  # Restart the api
  ssh admin_gunnrup_se@${array[i]} "
    cd /var/www/gunnrup.se/production/api
    npm install
    node_modules/.bin/forever restartall
  "
done
