#!/bin/sh
#########################################################
# Author: Hampus Gunnrup
# Date: 2018-07-30
# Updated: 2018-08-04
#
# This script installs all server parts needed for the gunnrup.se website
# on an Ubuntu server (16.04).
# This script assumes that the installation process described in the repository
# README is followed, which includes cloning the repository to the home directory
# and running this script from there.
# 
#########################################################

# Get the token credentials first, so that the script can run without any interuptions
sudo clear
read -p "Enter the deploy token username: " tokenUsername
read -p "Enter the deploy token password: " tokenPassword

# Make sure that everything is updated
sudo apt-get update
sudo apt-get -y upgrade
# Remove unnecessary packages
sudo apt -y autoremove

# Get the path to this script
SCRIPT_DIR="${0%/*}"

# Install the basic server components
. "$SCRIPT_DIR"/install-scripts/basic-server-setup.sh
cd ~

# Install the webserver components
. "$SCRIPT_DIR"/install-scripts/web-server-setup.sh
cd ~

# Install the front-end
. "$SCRIPT_DIR"/install-scripts/gunnrup.se-front-end-setup.sh
cd ~

# Install the database (needs to happen before the api is started, as it depends on the DB being installed on the same node)
. "$SCRIPT_DIR"/install-scripts/mongo-server-setup.sh
cd ~

# Install the back-end
. "$SCRIPT_DIR"/install-scripts/gunnrup.se-back-end-setup.sh
cd ~

# Set up the certificate (may be cancelled) - NEEDS TO BE LAST
read -p "Do you want to setup a certificate? (Answer Yes or No, default is No): " YES_NO
case $YES_NO in
  "Yes" ) . "$SCRIPT_DIR"/install-scripts/certificate-setup.sh;;
  "yes" ) . "$SCRIPT_DIR"/install-scripts/certificate-setup.sh;;
  "Y" ) . "$SCRIPT_DIR"/install-scripts/certificate-setup.sh;;
  "y" ) . "$SCRIPT_DIR"/install-scripts/certificate-setup.sh;;
  "No"  ) break;;
  *     ) break;;
esac


# TESTING - BEGIN

  # Test Suite 1: install-scripts/basic-server-setup.sh
  # Test Suite 2: install-scripts/web-server-setup.sh
  # Test Suite 3: install-scripts/gunnrup.se-front-end-setup.sh
  # Test Suite 4: install-scripts/mongo-server-setup.sh
  # Test Suite 5: install-scripts/gunnrup.se-back-end-setup.sh

# TESTING - END
