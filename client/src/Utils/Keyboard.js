/* eslint no-console: off */
import { isString, isInteger } from './TypeValidation';

export const ENTER_KEY = 'enter';
export const TAB_KEY = 'tab';
const tabKeyCode = 9;
const enterKeyCode = 13;

/**
 * Checks if the passed in event is a key press and matches the desired key.
 * For instance, calling the method like the following: eventIsKey(event, 'enter'),
 * returns true if the event registered an enter key press.
 *
 * Allowed values for the key parameter are:
 *  - 'enter'
 *  - 'tab'
 *  - any alphanumeric single letter
 *  - any integer representing a key code
 *
 * @param {object} event the event that is triggered (onkeyup, onkeydown, onkeypress)
 * @param {string|int} key the key to check for
 */
function eventIsKey(event, key) {
  // Try to retrieve the key and keyCode properties
  // from the event, otherwise set them to null.
  const eventKey = event ? event.key : null;
  const eventKeyCode = event ? event.code || event.which || event.keyCode || event.charCode : null;

  // If both the key and keyCode properties are missing,
  // return false
  if (!eventKey && !eventKeyCode) {
    return false;
  }

  if (isString(key)) {
    // const keyLower = key.toLowerCase();
    switch (key) {
      // Check if event matches the enter key
      case ENTER_KEY:
        return eventKey === 'Enter' || parseInt(eventKeyCode, 10) === parseInt(enterKeyCode, 10);

      // Check if the event matches the tab key
      case TAB_KEY:
        return eventKey === 'Tab' || parseInt(eventKeyCode, 10) === parseInt(tabKeyCode, 10);

      // If the key to check for is not in the supported list,
      // return false
      default:
        return false;
    }
  // If a key code is passed in,
  // return true if it matches the key event's
  // key code property
  } else if (isInteger(key)) {
    // The parseInt's are needed as they ensure the === works
    return parseInt(eventKeyCode, 10) === parseInt(key, 10);
  }

  // If all else fails, the function returns false
  return false;
}

/**
 * igher order function that returns a function that,
 * when called triggers the passed in function when an enter is hit.
 * @param {func} func the function to call if the returned function receives a enter key event.
 */
export default function triggerOnEnter(func) {
  const fallBackFunction = () => {
    console.error('The passed in function is not a function.');
  };

  return (event) => {
    if (eventIsKey(event, ENTER_KEY)) {
      // Function check suggested by: https://stackoverflow.com/a/7356528
      if (func && {}.toString.call(func) === '[object Function]') {
        func();
      } else {
        fallBackFunction();
      }
    }
  };
}

// Export the utility functions for testing,
// and for possible usage outside this module.
export { eventIsKey };
