export const serverErrorMessage = 'I cannot accept any messages at this moment.';
export const messageSentConfirmation = 'Thank you for your message! I will get back to you as soon as I can.';
export const fallbackErrorMessage = 'Something went wrong.';
export const errorMessageAlways = 'Note that you can always email me at gunnrup@gmail.com.';
