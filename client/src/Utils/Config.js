/**
 * This module abstracts the handling of configuration variables.
 * It decides what sources to base the configuration on, and
 * what precedence each source have.
 */

// This is the default configuration.
// If a field is not found in the
// configuration sources the deafult configuration's
// value is used for that field.
const defaultConfig = {
  apiLocation: process.env.REACT_APP_API_LOCATION || '/api',
};

// Try to read the config object from the
// global variables of the app.
// If it does not exist set it to an empty object.
const globalConfig = AppGlobal ? AppGlobal.config : {};

// Merge the configuration sources, resulting in each
// object to the right overriding the one to the left.
export default Object.assign({}, defaultConfig, globalConfig);
