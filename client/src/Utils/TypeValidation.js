/**
 * Checks if the passed in value is a string.
 * Returns true if it is, and false if not.
 *
 * @param {any} value the value to check
 */
function isString(value) {
  return typeof value === 'string' || value instanceof String;
}

/**
 * Checks if the passed in value is an integer.
 * Returns true if it is, and false if not.
 *
 * @param {any} value the value to check
 */
function isInteger(value) {
  return Number.isInteger(value);
}

export { isString, isInteger };
