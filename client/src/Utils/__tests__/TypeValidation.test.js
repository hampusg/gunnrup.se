import {
  isString,
  isInteger,
} from '../TypeValidation';

/**
 * isString tests.
 */
describe('isString tests: ', () => {
  // Test if string is detected as string
  it('detects strings', () => {
    const testString = 'flower';
    const testString2 = 'f';
    const testString3 = 'flower 20 null';
    const testString4 = '';
    expect(isString(testString)).toBe(true);
    expect(isString(testString2)).toBe(true);
    expect(isString(testString3)).toBe(true);
    expect(isString(testString4)).toBe(true);
  });

  // Test if array of characters returns false
  it('returns false for an array of characters', () => {
    const testArray = ['f', 'l', 'o', 'w', 'e', 'r'];
    expect(isString(testArray)).toBe(false);
  });

  // Test if an array of various types returns false
  it('returns false for mixed array', () => {
    const testArray = ['flower', null, undefined, 20];
    expect(isString(testArray)).toBe(false);
  });

  // Test if int returns false
  it('returns false for an integer', () => {
    const testInteger = 20;
    expect(isString(testInteger)).toBe(false);
  });

  // Test if null returns false
  it('returns false for null', () => {
    const testNull = null;
    expect(isString(testNull)).toBe(false);
  });

  // Test if function returns false
  it('returns false for function', () => {
    const testFunction = () => 'flower';
    expect(isString(testFunction)).toBe(false);
  });
});

describe('isInteger tests', () => {
  // Test if integer is detected as integer
  it('detects integers', () => {
    const testInteger = 20;
    const testInteger2 = 2003;
    const testInteger3 = 1;
    const testInteger4 = -123;
    const testInteger5 = 0;

    expect(isInteger(testInteger)).toBe(true);
    expect(isInteger(testInteger2)).toBe(true);
    expect(isInteger(testInteger3)).toBe(true);
    expect(isInteger(testInteger4)).toBe(true);
    expect(isInteger(testInteger4)).toBe(true);
    expect(isInteger(testInteger5)).toBe(true);
  });

  // Test if array of integers returns false
  it('returns false for an array of integers', () => {
    const testArray = [20, 2003, 1, -123, 0];
    expect(isInteger(testArray)).toBe(false);
  });

  // Test if an array of various types returns false
  it('returns false for mixed array', () => {
    const testArray = ['flower', null, undefined, 20];
    expect(isInteger(testArray)).toBe(false);
  });

  // Test if string returns false
  it('returns false for strings', () => {
    const testString = 'flower';
    const testString2 = '2';
    expect(isInteger(testString)).toBe(false);
    expect(isInteger(testString2)).toBe(false);
  });

  // Test if null returns false
  it('returns false for null', () => {
    const testNull = null;
    expect(isInteger(testNull)).toBe(false);
  });

  // Test if function returns false
  it('returns false for function', () => {
    const testFunction = () => 3;
    expect(isInteger(testFunction)).toBe(false);
  });
});
