import fetch from 'jest-fetch-mock';
import { sendMessage } from '../ApiProxy';
import { serverErrorMessage, fallbackErrorMessage } from '../Language';

// Mock the global fetch object.
global.fetch = fetch;

/**
 * sendMessage() tests.
 */
describe('sendMessage tests: ', () => {
  beforeEach(() => {
    // The following makes calls to console do nothing.
    // We want this, because console logging can obscure
    // the test output.
    global.console = {
      log: () => {},
      info: () => {},
      warn: () => {},
      error: () => {},
    };

    fetch.resetMocks();
  });

  // Test if the function returns a promise that is resolved,
  // if the fetch call returns a successful response with any data.
  it('returns successfully for normal request', async () => {
    // The test should return only one assertion
    expect.assertions(1);

    // Mock the response with using the object we expect to receive
    const expectObject = { data: 'test' };
    fetch.mockResponseOnce(JSON.stringify(expectObject));

    // Assert the async call
    await expect(sendMessage('', '', '')).resolves.toEqual(expectObject);
  });

  // If the fetch request returns an error with an existing message attribute,
  // the function should reject the promise with a its own message
  // and not the one returned from the fetch request.
  it('can handle fetch errors', async () => {
    // The test should return only one assertion
    expect.assertions(1);

    // Mock the response with using an error
    const errorMessage = 'The server is currently unavailable.';
    fetch.mockReject(new Error(errorMessage));

    // Assert the async call.
    // We expect it to be rejected and to contain the server message
    try {
      await sendMessage('', '', '');
    } catch (error) {
      expect(error.message).toEqual(serverErrorMessage);
    }
  });

  // If the fetch returns succefully but the response is not ok
  // the function should reject the promise with the response from the server
  // as the error.
  it('can handle internal server error', async () => {
    // The test should return only one assertion
    expect.assertions(1);

    // The messages used in the response and assert
    const serverMessage = 'The message could not be received.';

    // Mock the response
    fetch.mockResponseOnce(
      JSON.stringify({
        message: serverMessage,
      }),
      { status: 500 },
    );

    // Assert the async call.
    try {
      await sendMessage('', '', '');
    } catch (error) {
      expect(error.message).toEqual(serverMessage);
    }
  });

  // If the server responds with validation errors,
  // it should display reject with the message from the server.
  it('can handle server validation errors', async () => {
    // The test should return only one assertion
    expect.assertions(1);
    const serverMessage = 'The message you sent was incorrect. Please check the information you entered and try again.';

    // Mock the response
    fetch.mockResponseOnce(
      JSON.stringify({
        error: 'Invalid request: missing parameters',
        message: serverMessage,
      }),
      { status: 422 },
    );

    // Assert the async call.
    try {
      await sendMessage('', '', '');
    } catch (error) {
      expect(error.message).toEqual(serverMessage);
    }
  });

  // If the fetch returns succefully but the response is not ok (as the above)
  // and the server did not respond with any readable message,
  // the function should return a custom default error message as the error.
  it('can handle missing message', async () => {
    // The test should return only one assertion
    expect.assertions(1);

    // Mock the response
    fetch.mockResponseOnce(JSON.stringify({
    }), { status: 500 });

    // Assert the async call.
    try {
      await sendMessage('', '', '');
    } catch (error) {
      expect(error.message).toEqual(fallbackErrorMessage);
    }
  });

  // If the function gets called with insufficient parameters
  // it is a coding error not a user mistake.
  // Therefore the function should return the default
  // error message (as a reject).
  it('can handle missing parameters', async () => {
    // The test should return only one assertion
    expect.assertions(1);

    // Mock the response (even if it is not needed as the function wont call fetch)
    fetch.mockResponseOnce(
      JSON.stringify({
        data: 'This message should not be received! The sendMessage function should reject this case.',
      }),
      { status: 200 },
    );

    // Assert the async call.
    try {
      await sendMessage('', '');
    } catch (error) {
      expect(error.message).toEqual(fallbackErrorMessage);
    }
  });

  // it('returns default message if api location variable is bad');
});
