import triggerOnEnter, {
  eventIsKey,
  ENTER_KEY,
  TAB_KEY,
} from '../Keyboard';


describe('triggerOnEnter tests: ', () => {
  // The passed in function should be triggered when we pass in enter events.
  // We test a few variations to make sure it works for all cases we want to support.
  it('triggers on enter', () => {
    // Mock a stub function.
    const testFunction = jest.fn();

    // Call the method to retrieve a callback.
    const callback = triggerOnEnter(testFunction);

    // Call the callback with a enter keyboard event in a few variations.
    // We expect this to call the mocked function everytime.
    callback(new KeyboardEvent('keydown', { key: 'Enter', code: 13 }));
    callback(new KeyboardEvent('keydown', { key: 'Enter' }));
    callback(new KeyboardEvent('keydown', { code: 13 }));
    callback(new KeyboardEvent('keydown', { which: 13 }));
    callback(new KeyboardEvent('keydown', { keyCode: 13 }));
    callback(new KeyboardEvent('keydown', { charCode: 13 }));

    // Assert whether the mocked function has been called the right amount of times.
    expect(testFunction).toHaveBeenCalledTimes(6);
  });

  // The passed in function should not be triggered when we pass in other events.
  // Again, we test a few variations to make sure it works for all cases we want to support.
  it('does not trigger on other keys', () => {
    // Mock a stub function.
    const testFunction = jest.fn();

    // Call the method to retrieve a callback.
    const callback = triggerOnEnter(testFunction);

    // Call the callback with a few keys that are NOT enter.
    // We expect this to not do anything.
    callback(new KeyboardEvent('keydown', { key: 'Tab', code: 9 }));
    callback(new KeyboardEvent('keydown', { key: 'f', code: 70 }));
    callback(new KeyboardEvent('keydown', { code: 70 }));
    callback(new KeyboardEvent('keydown', { code: 23 }));

    // Assert whether the mocked function has been called.
    expect(testFunction).not.toHaveBeenCalled();
  });

  // The function should be able to handle keyboard events that are null.
  // In that case, it should simply not call the passed in function.
  it('returns false when the keyboard event is null', () => {
    // Mock a stub function.
    const testFunction = jest.fn();

    // Call the method to retrieve a callback.
    const callback = triggerOnEnter(testFunction);

    // Call the callback with a few types that are not events.
    // We expect this to not do anything.
    callback(null);
    callback(undefined);
    callback('string');
    callback(new Error('Error message'));

    // Assert whether the mocked function has been called.
    expect(testFunction).not.toHaveBeenCalled();
  });

  // The callback function should be filled with an empty one if the function
  // received by triggerOnEnter is not of type function.
  it('returns false if function is not a function', () => {
    // The following makes calls to console do nothing.
    // We want this, because console logging can obscure
    // the test output.
    global.console = {
      log: () => {},
      info: () => {},
      warn: () => {},
      error: () => {},
    };

    // Call the method to retrieve a callback.
    const callback = triggerOnEnter(null);

    // Call the callback with a few keys that are NOT enter.
    // We expect this to not do anything.
    // If the callbacks throw an error, the test fails.
    callback(new KeyboardEvent('keydown', { key: 'Enter', code: 13 }));
    callback(new KeyboardEvent('keydown', { key: 'Enter' }));
    callback(new KeyboardEvent('keydown', { code: 13 }));
    callback(new KeyboardEvent('keydown', { which: 13 }));
    callback(new KeyboardEvent('keydown', { keyCode: 13 }));
    callback(new KeyboardEvent('keydown', { charCode: 13 }));
  });
});

/**
 * eventIsKey tests.
 */
describe('eventIsKey tests: ', () => {
  // Test if an enter key event can be detected
  it('detects the enter key', () => {
    const event = new KeyboardEvent('keydown', { code: 13, key: 'Enter' });
    const equalTo = ENTER_KEY;
    expect(eventIsKey(event, equalTo)).toBe(true);
  });

  // Test if a tab key event can be detected
  it('detects the tab key', () => {
    const event = new KeyboardEvent('keydown', { code: 9, key: 'Tab' });
    const equalTo = TAB_KEY;
    expect(eventIsKey(event, equalTo)).toBe(true);
  });

  it('detects key codes', () => {
    const event = new KeyboardEvent('keydown', { code: 9, key: 'Tab' });
    const event2 = new KeyboardEvent('keydown', { code: 70, key: 'f' });
    const equalTo = 9;
    const equalTo2 = 70;
    expect(eventIsKey(event, equalTo)).toBe(true);
    expect(eventIsKey(event, equalTo2)).toBe(false);
    expect(eventIsKey(event2, equalTo)).toBe(false);
    expect(eventIsKey(event2, equalTo2)).toBe(true);
  });

  // Check if the function returns false if a key that has an inccorect type is passed in
  it('detects incorrect types', () => {
    const event = new KeyboardEvent('keydown', { code: 13, key: 'Enter' });
    const equalTo1 = null;
    const equalTo2 = undefined;
    const equalTo3 = ['f', 'l', 'o', 'w', 'e', 'r'];
    const equalTo4 = { key: ENTER_KEY };

    expect(eventIsKey(event, equalTo1)).toBe(false);
    expect(eventIsKey(event, equalTo2)).toBe(false);
    expect(eventIsKey(event, equalTo3)).toBe(false);
    expect(eventIsKey(event, equalTo4)).toBe(false);
  });

  // Check if the function can handle broken key events
  // (i.e. missing key, missing code or not of type KeyboardEvent)
  it('can handle broken keyevents', () => {
    const unspecifiedKeyEvent = new KeyboardEvent(null);
    const noKeyKeyEvent = new KeyboardEvent('keydown', { code: 13 });
    const noCodeKeyEvent = new KeyboardEvent('keydown', { key: 'Enter' });
    const equalTo1 = ENTER_KEY;
    const equalTo2 = 13;

    // If the keyboard event is unspecified (broken),
    // the function should always return false
    expect(eventIsKey(unspecifiedKeyEvent, equalTo1)).toBe(false);
    expect(eventIsKey(unspecifiedKeyEvent, equalTo2)).toBe(false);

    // If the keyboard event is missing the key property
    // the function should be able to use the key code property
    // when passing in key constants
    expect(eventIsKey(noKeyKeyEvent, equalTo1)).toBe(true);

    // If the keyboard event is missing the key property,
    // the function should still work when passing in key codes
    expect(eventIsKey(noKeyKeyEvent, equalTo2)).toBe(true);

    // If the keyboard event is missing the keyCode property,
    // the function should still work when passing in key constants
    expect(eventIsKey(noCodeKeyEvent, equalTo1)).toBe(true);

    // As of now the function does not, and perhaps should not, support
    // the case where the key code is missing in the event,
    // and a key code is passed in. However, it should still return false
    // and not throw an error.
    expect(eventIsKey(noCodeKeyEvent, equalTo2)).toBe(false);
  });

  it('supports older browsers', () => {
    const whichEvent = new KeyboardEvent('keydown', { which: 13 });
    const keyCodeEvent = new KeyboardEvent('keydown', { keyCode: 13 });
    const charCodeEvent = new KeyboardEvent('keydown', { charCode: 13 });
    const key = ENTER_KEY;
    const code = 13;

    expect(eventIsKey(whichEvent, key)).toBe(true);
    expect(eventIsKey(whichEvent, code)).toBe(true);
    expect(eventIsKey(keyCodeEvent, key)).toBe(true);
    expect(eventIsKey(keyCodeEvent, code)).toBe(true);
    expect(eventIsKey(charCodeEvent, key)).toBe(true);
    expect(eventIsKey(charCodeEvent, code)).toBe(true);
  });
});
