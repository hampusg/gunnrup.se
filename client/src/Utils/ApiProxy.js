/* eslint import/prefer-default-export: off, no-console: off */
import { serverErrorMessage, fallbackErrorMessage } from './Language';
import { isString } from './TypeValidation';
import config from './Config';

// The location of the api to send requests to
const { apiLocation } = config || '/api';

/**
 * This function returns a promise that will resolve if the server
 * responds with a status of 200.
 * In any other case it will reject the promise with an error object
 * containing at least a message property.
 *
 * @param {string} name the name of the sender
 * @param {string} email the email of the sender
 * @param {string} message the message the sender wants to send
 * @returns {Promise}
 */
export function sendMessage(name, email, message) {
  return new Promise((resolve, reject) => {
    // If anything that is not a string is passed in as a parameter
    // we should reject the promise with a default message.
    // This is because we want the user to see this message.
    // For the developer, we log an error to the console instead.
    if (!isString(name) || !isString(email) || !isString(message)) {
      console.error('Invalid Parameters.');
      reject(new Error(fallbackErrorMessage));
    }

    // Make a fetch (POST) request to the server endpoint apiLocation/messages
    fetch(`${apiLocation}/messages`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      mode: 'cors',
      body: JSON.stringify({
        name,
        email,
        message,
      }),
    }).then((response) => {
      // If the response is ok (i.e. not 200 something)
      if (response.ok) {
        resolve(response.json());
      // If the fetch resolves the promise
      // but the server returns something that is not "ok",
      // we should reject our promise with the message from the server.
      } else {
        // We first have to parse the json response.
        response.json()
          .then(error =>
            // Reject with at least a message property.
            // That property will default to fallBackErrorMessage,
            // and if the error object contains that property,
            // it will override the default one.
            reject(Object.assign({ message: fallbackErrorMessage }, error)))
          .catch(() => reject(new Error(serverErrorMessage)));
      }
    // If the response fails completely,
    // always reject with a custom error message.
    }).catch(() => {
      reject(new Error(serverErrorMessage));
    });
  });
}
