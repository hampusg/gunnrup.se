import React, { Component } from 'react';

import './AboutPage.css';

class AboutPage extends Component {
  render() {
    const element = (
      <div id="content">
        <div id="infobox">
          <div id="title">
            <span>Hi!</span>
          </div>
          <div id="text">
            <h3>Who am I?</h3>
            <p>
              My name is Hampus. I&apos;m a developer
              with a bachelor&apos;s in software engineering.
              Actually, I am more than that.
              I love creating software and the processes that go behind it.
              Right now I work as a web developer, but my interests go beyond that.
              I like the idea of being a full stack developer.
              I also love switching roles. There is nothing more satisfying than
              a well-oiled software process.
            </p>
            <h3>About this website</h3>
            <p>
              This website is a personal project that I created
              because I wanted to practice the process
              of developing a website with new technologies.
              Therefore, I decided to build the
              front end entirely in <a href="https://reactjs.org/">React</a> (using create react app).
              The back end API is build using <a href="https://expressjs.com/">ExpressJS</a> with <a href="https://www.mongodb.com/">MongoDB</a> as the main database.
              My aim is to be a full stack javascript developer.
              This fits nicely since I am familiar with javascript in general.
              <br />
              There is a lot of work that has been put on this website that is not visible.
              For instance, I designed the website using UML,
              specified the requirements with use case descriptions,
              assured the quality of the website using unit tests and created a complete
              pipeline that automatically deploys my latest
              repository commits to a staging environment.
            </p>
            <h3>My background</h3>
            <p>
              During my three years of university studies, I was introduced to courses in
              <strong> Linux</strong>, <strong>Cloud Computing</strong>,
              <strong> Software Architecture</strong>, <strong>Model Driven Development</strong>
              , <strong>Distributed Systems</strong>,
              <strong> Technical Analysis and Design </strong>
              and <strong>ER Modeling</strong>. The list is much longer, but you get the idea.
              Besides the courses, I learned how to work within a team using Scrum.
              Lastly, since each semester had one large ongoing project I learned how to apply
              my knowledge to something more concrete.
            </p>
            <h3>My work process</h3>
            <p>
              Personally, I use a combination of scrum and test-driven development.
              Firstly, I create a user story and a git branch for the story/feature.
              I then create a simple use case description (based on a document template)
              for the story and a basic wireframe sketch using photoshop.
              Depending on the importance of the user story,
              I might make some alternate paths for the use case and respective wireframe sketches.
              I now move on to the software design by
              updating the architecture (UML diagrams) in papyrus.
              After making the design of the feature I move on to making
              unit tests that correspond to the requirements (use case) of the
              feature and stubs that correspond to the module structure in the architecture.
              After adding a unit test,
              I implement the code that satisfies that unit test.
              I repeat this cycle until I can&apos;t think of any more tests
              (or if I determine that no more tests are necessary).
              When I am satisfied with my implementation
              I merge the branch to master and watch the pipeline do its thing.
              I tag the project in a MAJOR.MINOR.PATCH manner
              (MAJOR for incompatibilities,
              MINOR for backward compatible features and PATCH for minor fixes).
              Finally, when I&apos;ve tested the commit(s) in the staging environment
              I deploy my changes to production.
              <br />
              Note that this process is tuned to fit a one-man team.
              It is simply too time-consuming to do a full scrum process,
              test every inch of code and design the most precise software known to man.
              Besides, one person developing equals less organizational
              issues thus making some parts of software engineering irrelevant.
            </p>
            <p>
              I hope I didn&apos;t bore you with my rambling,
              but I feel that by sharing these things you gain a
              good understanding of who I am and what I am capable of,
              and you can, therefore, decide if I&apos;m worth contacting.
              <br />
              <br />
              Cheers!
            </p>
          </div>
        </div>
      </div>
    );

    return element;
  }
}

export default AboutPage;
