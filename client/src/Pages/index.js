export { default as Index } from './IndexPage/IndexPage.jsx';
export { default as About } from './AboutPage/AboutPage.jsx';
export { default as MyWork } from './MyWorkPage/MyWorkPage.jsx';
export { default as Contact } from './ContactPage/ContactPage.jsx';
