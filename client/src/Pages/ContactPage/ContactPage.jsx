import React, { Component } from 'react';

import { sendMessage } from '../../Utils/ApiProxy';
import './ContactPage.css';
import { messageSentConfirmation, fallbackErrorMessage, errorMessageAlways } from '../../Utils/Language';

class IndexPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      message: '',
      messageSent: false,
      errorMessage: null,
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handleMessageChange = this.handleMessageChange.bind(this);
  }

  handleNameChange(event) {
    const newValue = event.target.value;
    this.setState({ name: newValue });
  }

  handleEmailChange(event) {
    const newValue = event.target.value;
    this.setState({ email: newValue });
  }

  handleMessageChange(event) {
    const newValue = event.target.value;
    this.setState({ message: newValue });
  }

  handleSubmit(event) {
    const newState = {
      messageSent: false,
      errorMessage: null,
    };

    sendMessage(this.state.name, this.state.email, this.state.message)
      .then(() => {
        newState.messageSent = true;
        newState.name = '';
        newState.email = '';
        newState.message = '';
      })
      .catch((error) => {
        newState.errorMessage = error.message || fallbackErrorMessage;
      }).finally(() => this.setState(newState));


    event.preventDefault();
  }

  render() {
    const {
      handleSubmit,
      handleNameChange,
      handleEmailChange,
      handleMessageChange,
    } = this;

    const {
      name,
      email,
      message,
      messageSent,
      errorMessage,
    } = this.state;

    let messageConfirmation = null;
    if (messageSent) {
      messageConfirmation = (
        <div className="message positive">
          {messageSentConfirmation}
        </div>
      );
    }

    if (errorMessage !== null) {
      messageConfirmation = (
        <div className="message negative">
          <p>
            {errorMessage || fallbackErrorMessage} {errorMessageAlways}
          </p>
        </div>
      );
    }

    const element = (
      <div id="content">
        {messageConfirmation}
        <div id="infobox">
          <div id="title">
            <span>Contact me!</span>
          </div>
          <form id="contact-form" align="center" onSubmit={handleSubmit}>
            <input
              type="text"
              name="firstname"
              placeholder="Name"
              value={name}
              onChange={handleNameChange}
            />
            <br />
            <input
              type="email"
              name="email"
              placeholder="E-mail address"
              value={email}
              onChange={handleEmailChange}
            />
            <br /><br />
            <br />
            <textarea
              name="message"
              placeholder="Message"
              value={message}
              onChange={handleMessageChange}
            />
            <br />
            <input type="submit" name="submit-message" />
          </form>
        </div>
      </div>
    );

    return element;
  }
}

export default IndexPage;
