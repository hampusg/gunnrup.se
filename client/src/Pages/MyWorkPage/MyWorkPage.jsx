import React, { Component } from 'react';

import './MyWorkPage.css';
import { Project } from '../../Components';

class IndexPage extends Component {
  render() {
    const element = (
      <div id="content">
        <div id="container">
          <Project url="https://github.com/johannes95/project_fall_semester_2015" title="CelebrityDeath#" />
          <Project url="https://github.com/SEM-PP-GROUP8/RestaurantFinder" title="RestaurantFinder" />
          <Project url="https://github.com/rafafloripa/OpenDaVINCI" title="Miniature Autonomous Car" />
          <Project url="https://github.com/johannes95/SE-M-Project-2015" title="AGA Project" />
        </div>
      </div>
    );

    return element;
  }
}

export default IndexPage;
