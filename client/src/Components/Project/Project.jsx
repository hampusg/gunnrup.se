import React from 'react';
import PropTypes from 'prop-types';

function handleKeyUp(event, url) {
  if (event.key === 'Enter') {
    window.open(url);
  }
}

export default class Project extends React.Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
    this.handleKeyUp = this.handleClick.bind(this);
  }

  handleClick() {
    window.open(this.props.url);
  }

  render() {
    return (
      <div onClick={this.handleClick} style={{ cursor: 'pointer' }} onKeyUp={event => handleKeyUp(event, this.props.url)} role="button" tabIndex={0}>
        <span>{this.props.title}</span>
      </div>
    );
  }
}

Project.propTypes = {
  title: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};
