import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './ImageShowcase.css';

/**
 * This component takes an array/list of images, and displays these images in a showcase.
 */
export default class ImageShowcase extends Component {
  constructor(props) {
    super(props);

    // Set the initial state
    this.state = {
      visibleElement: 0,
      currentImageIndex: 0,
      div1Style: {
        backgroundImage: `url(${this.props.images[0]})`,
      },
      div2Style: {
        backgroundImage: `url(${this.props.images[1]})`,
      },
    };
  }

  componentDidMount() {
    // Create an infinite loop,
    // that will load the next image in the showcase,
    // once every 6th seconds.
    this.timer = setInterval(
      () => this.loadNextImage(),
      6000,
    );
  }

  componentWillUnmount() {
    // When the component is removed/destroyed, the interval should also be removed
    clearInterval(this.timer);
  }

  /**
   * Returns the index (0 or 1) of the div to be transitioned to, during the next transition.
   * This function can be seen as a toggle between the two divs.
   *
   * This is a pure function.
   */
  nextVisible() {
    let visible = 0;

    if (this.state.visibleElement === 0) {
      visible = 1;
    }

    return visible;
  }

  /**
   * This function updates the state of the component, which leads to the next image transition.
   * In other words, this method causes the next image to load.
   *
   * This function is not pure.
   */
  loadNextImage() {
    // Load the next image.
    // But, if the next image is out of bounds,
    // Load the first image
    let nextIndex = this.state.currentImageIndex + 1;
    if (nextIndex > this.props.images.length - 1) {
      nextIndex = 0;
    }

    // Get the style (image) to be used on the next visible div.
    // If the first div is the currently visible element, keep the style,
    // otherwise load the next style.
    // The result is that the showcase will transition to the element with the nextStyle.
    const nextStyle = { backgroundImage: `url(${this.props.images[nextIndex]})` };
    const div1Style = this.state.visibleElement === 0 ? this.state.div1Style : nextStyle;
    const div2Style = this.state.visibleElement === 1 ? this.state.div2Style : nextStyle;

    /* The state change will trigger a re-render */
    this.setState({
      visibleElement: this.nextVisible(),
      currentImageIndex: nextIndex,
      div1Style,
      div2Style,
    });
  }

  render() {
    const element = (
      <div id="showcase">
        <div style={this.state.div1Style} className={this.state.visibleElement === 0 ? 'visible' : 'hidden'} />
        <div style={this.state.div2Style} className={this.state.visibleElement === 1 ? 'visible' : 'hidden'} />
      </div>
    );

    return element;
  }
}

ImageShowcase.propTypes = {
  images: PropTypes.arrayOf(PropTypes.string).isRequired,
};
