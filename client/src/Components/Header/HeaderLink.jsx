import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import './HeaderLink.css';

export default function HeaderLink(props) {
  return (
    <NavLink className="headerLink" activeClassName="currentLink" {...props}>
      <span>
        {props.children}
      </span>
    </NavLink>
  );
}

HeaderLink.propTypes = {
  children: PropTypes.node.isRequired,
};
