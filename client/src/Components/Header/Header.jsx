import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import HeaderLink from './HeaderLink';
import MobileMenu from '../MobileMenu/MobileMenu';

import './Header.css';

/* function handleKeyUp() {
  // FIXME: make handler for keyup
  console.log('MobileMenu -> #menuclose -> key up');
} */

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isMobile: window.getDeviceType() >= 2,
      mobileMenuVisible: false,
    };

    this.toggleMenu = this.toggleMenu.bind(this);
  }

  componentDidMount() {
    window.addEventListener('resize', () => this.setState({ isMobile: window.getDeviceType() >= 2 }));
  }

  toggleMenu() {
    this.setState({ mobileMenuVisible: !this.state.mobileMenuVisible });
  }

  render() {
    const desktopHeader = (
      <header>
        <Link to="/">
          <div id="headerlogo">
            <div><span id="logotexthg">Hampus</span></div>
            <div><span id="logotextwebsite">Gunnrup</span></div>
          </div>
        </Link>
        <div id="headermenu">
          <ul>
            <li className="noborder">
              <HeaderLink to="/about">About me</HeaderLink>
            </li>
            <li>
              <HeaderLink to="/work">My work</HeaderLink>
            </li>
            <li>
              <HeaderLink to="/contact">Contact</HeaderLink>
            </li>
          </ul>
        </div>
      </header>
    );

    const mobileHeader = (
      <header>
        <Link to="/">
          <div id="headerlogo">
            <div><span id="logotexthg">Hampus</span></div>
            <div><span id="logotextwebsite">Gunnrup</span></div>
          </div>
        </Link>
        <div
          id="mobilemenuicon"
          onClick={this.toggleMenu}
          onKeyUp={this.toggleMenu}
          role="button"
          tabIndex={0}
        >
          <div id="mobilemenutext"><span>MENU</span></div>
          <div id="line1" className="mobilemenuline" />
          <div id="line2" className="mobilemenuline" />
          <div id="line3" className="mobilemenuline" />
        </div>
        <MobileMenu visible={this.state.mobileMenuVisible} onClose={this.toggleMenu} />
      </header>);

    return !this.state.isMobile ? desktopHeader : mobileHeader;
  }
}

export default Header;
