import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './Body.css';

export default class Body extends Component {
  render() {
    const element = (<div id="body">{this.props.children}</div>);
    return element;
  }
}

Body.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
};
