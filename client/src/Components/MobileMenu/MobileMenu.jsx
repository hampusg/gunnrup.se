import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

import './MobileMenu.css';
import triggerOnEnter from '../../Utils/Keyboard';

export default function MobileMenu(props) {
  const { onClose, visible } = props;
  const displayProperty = visible ? 'inline' : 'none';

  const element = (
    <div id="mobilemenuwrapper" style={{ display: displayProperty }}>
      <div
        id="menuclose"
        onClick={onClose}
        onKeyUp={triggerOnEnter(onClose)}
        role="button"
        tabIndex={0}
      >
        <span>X</span>
      </div>
      <div id="menubox">
        <ul>
          <NavLink
            onClick={onClose}
            activeClassName="currentMobile"
            to="/about"
          >
            <li className="radiustop">
              <span>About me</span>
            </li>
          </NavLink>
          <NavLink
            onClick={onClose}
            activeClassName="currentMobile"
            to="/work"
          >
            <li>
              <span>My work</span>
            </li>
          </NavLink>
          <NavLink
            onClick={onClose}
            activeClassName="currentMobile"
            to="/contact"
          >
            <li className="noborder radiusbottom">
              <span>Contact</span>
            </li>
          </NavLink>
        </ul>
      </div>
    </div>
  );

  return element;
}

MobileMenu.propTypes = {
  visible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};
