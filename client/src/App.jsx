import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import './App.css';
import { Index, About, MyWork, Contact } from './Pages';
import { Header, Body, ImageShowcase } from './Components';

/* Fetch the images (references) for the showcase */
import i1 from './Images/ImageShowcase/1.JPG';
import i2 from './Images/ImageShowcase/2.JPG';
import i3 from './Images/ImageShowcase/3.JPG';
import i4 from './Images/ImageShowcase/4.JPG';
import i5 from './Images/ImageShowcase/5.JPG';

class App extends Component {
  constructor(props) {
    super(props);

    window.getDeviceType = function getDeviceType() {
      const element = document.getElementById('deviceTest');
      return element ? window.getComputedStyle(element).getPropertyValue('z-index') : 1;
    };
  }

  render() {
    return (
      <BrowserRouter>
        <div id="app">
          <Header />
          <Body>
            <ImageShowcase images={[i1, i2, i3, i4, i5]} />
            <Route exact path="/" component={Index} />
            <Route path="/about" component={About} />
            <Route path="/work" component={MyWork} />
            <Route path="/contact" component={Contact} />
          </Body>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
