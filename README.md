### What is this repository for? ###
This is a repository for the gunnrup.se website. It contains the client code, api code and the documentation. Further, it contains the pipeline configurations needed to stage and deploy the website.

### How do I get set up? ###
First install the dependencies at the root by running `npm install`.
You can then install the api and/or client projects separately by running `cd api && npm install` and `cd client && npm install` respectively.

After installing the necessary dependencies, run npm start in the root directory.
This will start the api on port 3001 and the front-end app will be served at port 3000.
Changes to files in the client will be automatically handled and the browser tab will be refreshed.

If you want changes to api files to be watched as well, go into the api directory and run npm start.

Note that the api port can be configured by setting the environment variable PORT before starting the api.
Likewise, the client can be configured to use a different api path than the default one (/api). This is configured
by setting the environment variable REACT_APP_API_LOCATION.

For example, if you install the project on a linux machine you can run `cd api && PORT=3002 npm start` if you want to start the api on port 3002.
You can then run `cd client && REACT_APP_API_LOCATION=localhost:3002/api npm start` to make the client use the new location of the api.

### How should a normal workflow look? ###
Firstly, a user story should be created in the backlog https://gitlab.com/gunnrup/gunnrup.se/boards.
You can use the following template as a starting point: https://gitlab.com/gunnrup/gunnrup.se/issues/6.
Make sure that the user story (issue) is assigned to you.

After a user story has been created, a simple use case with a brief description should be created.
If the user story involves more complicated interactions, alternate flows should be created.
The use case descriptions can initially be very simple. Make sure you also add the use case to the use case diagram.

After completing the use case description, the feature should be roughly designed in the architecture project (papyrus) that resides in this repository.
It is important to also update the textual descriptions of the diagrams you modify or add a new document if you create a new diagram.
There are appropriate templates for both use case descriptions and the architecture diagram descriptions under the Documentation/Document Templates
directory of this repository.

You should now move on to creating a UI mockup of the feature.

Finally the actual implementation should be performed. This should be done in a TDD manner:
 * Make a test that covers some part of the use case(s).
 * Implement code that makes the test pass.
 * Repeat until all use cases are covered.

The architecture should be revised along with the implementation. You have a lot of freedome over how you do this, as long as the architecture is in sync with the implementation in the end.

When working on a new bug fix or feature, create a new branch and work on everything locally.
After you feel confident about your work you may merge the branch into master and delete the branch.
The website and api will be deployed to the staging environment automatically after pushing the new master branch.
You may now navigate to staging.gunnrup.se make sure that everything works.


### How do I contribute? ###
Make sure that you are invited to this repository, so that you can push your commits.
Use the described workflow when you contribute.
Happy hacking!

### How do I deploy my contributions to the staging environment? ###
Every commit that is pushed to the master branch of the repository, is automatically tested and deployed to the staging environment.
Go to staging.gunnrup.se to see your contributions in action.

### How do I deploy my contributions to the production environment? ###
After you have tested your contributions in the staging environment, you may update the version field in package.json file.
This should ideally be the only change in the commit.
Further, that commit should be tagged with the same version that was added to the package.json file (i.e. v[major].[minor].[patch]).

Note that the new version has two levels. Firstly, you should increment the subproject version.
Secondly, you should incremente the root project version the same amount.
For instance, lets say that you make a bug fix and increment the client version from 1.2.3 to 1.2.4, then you would have to update the root project
from, say, 2.3.4 to 2.3.5. This way the subprojects of this repository keep their own versions, and the root project keeps the overall status of all projects.

The root project also has its own version. This version should be updated when new scripts are added, bugs are fixed in those scripts or if new packages
are added to the root. The root project version is not as important as the subproject versions, but it serves as a release candidate and revert point.

Note that changes that do not modify the integrity of the project (i.e. comments and documentation), do not need to be tagged and should ideally contain
[ci skip] in the commit message. This will stop the pipeline from running.

### What version should I tag with? ###

Given a version number MAJOR.MINOR.PATCH, increment the:

MAJOR version when you make incompatible changes,
MINOR version when you add features in a backwards-compatible manner, and
PATCH version when you make backwards-compatible bug fixes and smaller additions.

The version field in the package.json file should be "MAJOR.MINOR.PATCH".
The git tag version should be vMAJOR.MINOR.PATCH.

### How do I add a subdomain? ###
To add a subdomain, you need access to the dns service (namecheap.com). Add an A record for the subdomain pointing to the server.
You then need to add a virual host in an appropriate apache site config file, and run `a2enmod <yournewconfigname>`. Lastly run `apachectl -k graceful` or `service apache2 reload` in order for apache to load the new config.

Take a look at the apache configuration files in the repository https://gitlab.com/gunnrup/configuration for some examples.

### How do I add SSL for a subdomain? ###
Simply run the command `certbot --apache` and make sure to choose the new subdomain (the program will automatically find it).
For more information visit https://certbot.eff.org/lets-encrypt/ubuntuxenial-apache and https://certbot.eff.org/docs/.

### How do I install software to the server? ###
In order to access the server, you need the ssh key pair file. Once you have access, you can simply install the software.
However, you are required to add the installation steps to the appropriate install script(s) under scripts/install-scripts and/or scripts/install-website.sh.
Those install scripts have two purposes. Firstly, they serve as a sort of documentation of what software is required to run the system.
Secondly, they can be used to install software on a new server (in the case of a migration).

### Can I work directly to the master branch? ###
In some cases when the changes you make are small, you may work directly on the master branch. This includes activities such as updates to the readme file or documentation,
bugfixes that involve fixing spelling mistakes, hot fixes that are important to push fast and comments applied to code.

### How do I install the website on a server? ###
This project can be installed on any system and along with any server. However, the supported method is an installation on an ubuntu server 16.04 using apache, node and mongodb.
In order to install the website on a new server through the supported way, simply do the following:
* Make sure you are in the home directory of your ubuntu 16.04 server: `cd ~`
* Clone this repository: `git clone https://gitlab.com/gunnrup/gunnrup.se.git`
* Run the main installation script: `gunnrup.se/scripts/install-website.sh`
* Follow the initial instructions (input password and deployment tokens)
* Wait until the script has finished
* Done!
* Optionally, you can use the individual scripts under gunnrup.se/scripts/install-scripts, to install a subset of the project

You might consider reloading the profile after the installation by running `. ~/.profile`, in order to avoid any confusion.
Alternatively, you can log out then in again or restart your ssh connection to the server.

You might want to add the server to the pipeline, so that new versions of the website are automatically added to your server.
For this, two things are required. Number one, your ubuntu 16.04 server authentication method needs to rely on the same ssh certificate key that the pipeline is using.
Number two, your server's ip address needs to be added to DEPLOY_SERVER variable in gitlab. This list is separated by commas (e.g. 54.93.248.184,[your-server-ip]).
Please contact an administrator for assistance, before conducting these steps.

### Who do I talk to? ###

* contact@gunnrup.se
* gunnrup@gmail.com
