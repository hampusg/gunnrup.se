/* eslint-disable */

/**
 * This script initializes the gunnrup database.
 * It creates the db, clears all collections
 * and creates the indexes.
 */

// Create (or get if it exists) the gunnrup database
db = new Mongo().getDB('gunnrup');

// Clear the database collections
db.messages.remove({});

// Create indexes
db.messages.createIndex({ name: 1 });
db.messages.createIndex({ email: 1 });
db.messages.createIndex({ message: "text" });
db.messages.createIndex({ ip: 1 });