// This consant contains the fields that are allowed,
// and specifies if a field is required or not.
const messageFields = {
  name: 'required',
  email: 'required',
  message: 'required',
  created: 'required',
};

const validateMessage = (message) => {
  const successObject = {
    success: true,
    message: '',
  };

  // Loop through all allowed fields
  Object.keys(messageFields).forEach((field) => {
    const fieldIsRequired = messageFields[field] === 'required';

    // If the current field is required
    // and the incoming message does not contain that field,
    // Return a message.
    if (fieldIsRequired && !message[field]) {
      successObject.success = false;
      successObject.message = `${field} is required.`;
    }

    return null;
  });

  return successObject;
};

module.exports = {
  validateMessage,
};
