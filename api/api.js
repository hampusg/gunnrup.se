/* eslint no-console: off, arrow-body-style: off */

const express = require('express');
const bodyParser = require('body-parser');
const { MongoClient } = require('mongodb');

const Message = require('./Message');

const app = express();
const port = process.env.PORT || 3001;
let db = null;
const databaseLocation = 'mongodb://localhost:27017';
const databaseName = 'gunnrup';
const allowedOrigins = [
  '//localhost',
  '//gunnrup.se',
  '//staging.gunnrup.se',
];

// Enable json parsing
app.use(bodyParser.json());
// Enable cross origin requests
app.use((req, res, next) => {
  const {
    origin,
  } = req.headers;

  // If the origin of the request is in the whitelist
  if (allowedOrigins.includes(origin)) {
    // Allow the origin to access the api
    res.header('Access-Control-Allow-Origin', origin);
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  }

  next();
});

// Handle the get request gunnrup.se/api/messages
app.get('/api/messages', (req, res) => {
  db.collection('messages').find().toArray()
    // When the database has returned the result of the find
    .then((messages) => {
      res.json({ records: messages });
    })
    // If the database produced an error
    .catch((error) => {
      console.log(error);
      res.status(500).json({ error: `Internal Server Error: ${error}`, message: 'The messages are currently unavailable.' });
    });
});

// Handle the post request gunnrup.se/api/messages
app.post('/api/messages', (req, res) => {
  // Create a new message object
  const newMessage = req.body;
  newMessage.created = new Date();

  // Validate the message.
  // If the validation fails,
  // Respond to the request with the message.
  const validationResult = Message.validateMessage(newMessage);
  if (!validationResult.success) {
    res.status(422).json({ error: `Invalid request: ${validationResult.message}`, message: 'The message you sent was incorrect. Please check the information you entered and try again.' });
    return;
  }

  // Add the message to the collection
  db.collection('messages').insertOne(newMessage)
    // When the database has returned the result of the insert
    .then((result) => {
      // Find the newly inserted message and call the next handler
      return db.collection('messages').find({ _id: result.insertedId }).limit(1).next();
    })
    // When the database has returned the result of the find
    .then((insertedMessage) => {
      res.json(insertedMessage);
    })
    // If the database produced an error
    .catch((error) => {
      console.log(error);
      res.status(500).json({ error: `Internal Server Error: ${error}`, message: 'The message could not be received.' });
    });
});

// Set up the database connection,
// and start the api.
MongoClient.connect(databaseLocation)
  .then((connection) => {
    db = connection.db(databaseName);
    app.listen(port, () => console.log(`API server listening on port ${port}`));
  })
  .catch((error) => {
    console.log('Error Connecting to Mongo: ', error);
  });
