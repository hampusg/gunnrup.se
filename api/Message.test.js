const Message = require('./Message');

const { validateMessage } = Message;

describe('validateMessage tests:', () => {
  it('returns successful when all keys are there', () => {
    const testMessage = {
      name: 'Hampus',
      email: 'gunnrup@gmail.com',
      message: 'Hi!',
      created: new Date(),
    };

    const expectedObject = {
      success: true,
      message: '',
    };

    expect(validateMessage(testMessage)).toEqual(expectedObject);
  });

  it('returns unsuccessful when not all keys are there', () => {
    const testMessage = {
      email: 'gunnrup@gmail.com',
      message: 'Hi!',
      created: new Date(),
    };
    const testMessage2 = {
      name: 'Hampus',
      message: 'Hi!',
      created: new Date(),
    };
    const testMessage3 = {
      name: 'Hampus',
      email: 'gunnrup@gmail.com',
      created: new Date(),
    };
    const testMessage4 = {
      name: 'Hampus',
      email: 'gunnrup@gmail.com',
      message: 'Hi!',
    };
    const testMessage5 = {
    };

    const testMessage6 = {
      lastName: 'Gunnrup',
    };

    const expectedObject = {
      success: false,
      message: 'name is required.',
    };
    const expectedObject2 = {
      success: false,
      message: 'email is required.',
    };
    const expectedObject3 = {
      success: false,
      message: 'message is required.',
    };
    const expectedObject4 = {
      success: false,
      message: 'created is required.',
    };

    expect(validateMessage(testMessage)).toEqual(expectedObject);
    expect(validateMessage(testMessage2)).toEqual(expectedObject2);
    expect(validateMessage(testMessage3)).toEqual(expectedObject3);
    expect(validateMessage(testMessage4)).toEqual(expectedObject4);
    expect(validateMessage(testMessage5)).toEqual(expectedObject4);
    expect(validateMessage(testMessage6)).toEqual(expectedObject4);
  });
});
