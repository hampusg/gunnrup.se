### About This Project ###
This is the api of the gunnrup.se project. It is built using ExpressJS and uses a REST based architecture.

### How do I contribute? ###
Firstly, the same rules and guidelines noted in the root of this repository apply for this subproject.
Secondly, to start developing you may simply run `npm install` and then `npm start`. After this changes to files trigger a restart of the api server.
The default port of the api is 3002, but you can change this if desired by running `PORT=3003 npm start` or whatever port you want the api to be served on.
However, the api assumes that mongodb is running on port 27017.

### How do I launch the api in production? ###
Simply run `npm run launch` to start a forever process.
